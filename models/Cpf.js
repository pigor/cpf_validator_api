module.exports = {
  checkCorrectSize: cpf => {
    return cpf.length == 11;
  },

  checkAllSameDigits: cpf => {
    let firstDigit = cpf.substring(0, 1);
    let index = 1;
    for (; index < 12; ++index) {
      if (firstDigit != cpf.toString().charAt(index)) {
        break;
      }

      index++;
    }

    return index == cpf.length;
  },

  validateDigit: (cpf, valueRef) => {
    const digits = cpf.substring(0, valueRef);
    const digitChecker = cpf.substring(valueRef, valueRef + 1);

    let total = null;
    for (var j = 0; j < valueRef; ++j) {
      total += digits.toString().charAt(j) * (valueRef + 1 - j);
    }

    const result = total % 11;
    const digit = result < 2 ? 0 : 11 - result;

    return digitChecker == digit;
  }
};
