require("appmetrics-dash").monitor();

const express = require("express");
const cors = require('cors');

const app = express();
const cpfValidator = require("./models/Cpf");

app.get("/", (req, res) =>
  res.send("API DevOps para validação de CPF. Exemplo: /validator/?cpf=1111111111")
);
app.get("/validator", cors(), (req, res) => {
  const cpf = req.query.cpf;
  const result =
    cpfValidator.checkCorrectSize(cpf) &&
    !cpfValidator.checkAllSameDigits(cpf) &&
    cpfValidator.validateDigit(cpf, 9) &&
    cpfValidator.validateDigit(cpf, 10);

  res.send({
    cpf: cpf,
    result: result
  });
});

app.listen(3000);
