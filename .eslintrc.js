module.exports = {
  parser: "babel-eslint",
  extends: ["eslint:recommended", "plugin:jasmine/recommended"],
  rules: {
    "linebreak-style": ["error", "unix"],
    code: 150,
    "no-unused-vars": ["error", { argsIgnorePattern: "^_" }]
  },
  plugins: ["jasmine"],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      impliedStrict: true
    }
  },
  env: {
    es6: true,
    browser: true,
    node: true,
    jasmine: true
  }
};
